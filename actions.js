import { get } from "./api.js";
import { delay } from "https://deno.land/std@0.148.0/async/delay.ts";

export async function getAction(token, actionId) {
  const { action } = await get(
    token,
    `/actions/${actionId}`,
  );
  return action;
}

export async function waitAction(token, actionId, delayMs = 5000) {
  let action;
  let status;
  do {
    action = await get(token, actionId);
    status = action.status;
    await delay(delayMs);
  } while (!["completed", "done"].includes(status));
  return action;
}
