import { post } from "./api.js";
import { waitAction } from "./actions.js";

async function dropletAction(token, dropletId, body = {}) {
  const { action: { id } } = await post(
    token,
    `/droplets/${dropletId}/actions`,
    {},
    body,
  );
  return await waitAction(token, id);
}

async function dropletActionByTag(token, tag_name, body = {}) {
  const { action: { id } } = await post(token, `/droplets/actions`, {
    tag_name,
  }, body);
  return await waitAction(token, id);
}

export async function shutdownDroplet(token, dropletId) {
  return await dropletAction(token, dropletId, { type: "shutdown" });
}

export async function snapshotDroplet(token, dropletId, name) {
  return await dropletAction(token, dropletId, { type: "snapshot", name });
}

export async function snapshotDropletByTag(token, tag) {
  return await dropletActionByTag(token, tag, { type: "snapshot" });
}

export async function powerOnDroplet(token, dropletId) {
  return await dropletAction(token, dropletId, { type: "power_on" });
}

export async function resizeDroplet(token, dropletId, size, disk = false) {
  return await dropletAction(token, dropletId, { type: "resize", size, disk });
}

export async function restoreDroplet(token, dropletId, image) {
  return await dropletAction(token, dropletId, { type: "restore", image });
}

export async function rebuildDroplet(token, dropletId) {
  return await dropletAction(token, dropletId, { type: "rebuild" });
}
