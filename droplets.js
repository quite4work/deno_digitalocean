import { get, post, remove } from "./api.js";

export async function createDroplet(
  token,
  name,
  region,
  size,
  image,
  opts = {},
) {
  const {
    ssh_keys,
    backups,
    ipv6,
    private_networking,
    vpc_uuid,
    user_data,
    monitoring,
    volumes,
    tags,
  } = opts;
  const { droplet, links } = await post(token, `/droplets`, {}, {
    name,
    region,
    size,
    image,
    ssh_keys,
    backups,
    ipv6,
    private_networking,
    vpc_uuid,
    user_data,
    monitoring,
    volumes,
    tags,
  });
  return { droplet, links };
}

export async function getDropletSnapshots(token, dropletId) {
  const { snapshots } = await get(token, `/droplets/${dropletId}/snapshots`);
  return snapshots;
}

export async function getDropletsByTag(token, tag_name) {
  const { droplets } = await get(token, `/droplets`, { tag_name });
  return droplets;
}

export async function getDroplets(token) {
  const { droplets } = await get(token, `/droplets`);
  return droplets;
}

export async function removeDropletByTag(token, tag_name) {
  return await remove(token, `/droplets`, { tag_name });
}
