import { get, remove } from "./api.js";

export async function getLoadBalancers(token) {
  const { load_balancers } = await get(token, `/load_balancers`);
  return load_balancers;
}

export async function removeLoadBalancer(token, lb_id) {
  return await remove(token, `/load_balancers/${lb_id}`);
}
