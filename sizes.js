import { get } from "./api.js";

export async function getDropletSizes(token, page = 1, per_page = 200) {
  const { sizes } = await get(token, `/sizes`, { page, per_page });
  return sizes;
}
