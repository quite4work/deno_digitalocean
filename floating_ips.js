import { get, post, remove as apiRemove } from "./api.js";

export async function getIps(token) {
  const { floating_ips } = await get(token, `/floating_ips`);
  return floating_ips;
}

export async function removeIp(token, ip) {
  return await apiRemove(token, `/floating_ips/${ip}`);
}

export async function createIp(token, region) {
  const { floating_ip } = await post(token, `/floating_ips`, {}, { region });
  return floating_ip;
}

export async function assignIp(token, ip, droplet_id) {
  const { action } = await post(token, `/floating_ips/${ip}/actions`, {}, {
    type: "assign",
    droplet_id,
  });
  return action;
}

export async function unassignIp(token, ip) {
  const { action } = await post(token, `/floating_ips/${ip}/actions`, {}, {
    type: "unassign",
  });
  return action;
}
