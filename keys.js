import { get, post, remove } from "./api.js";

export async function getKeys(token) {
  const { ssh_keys } = await get(token, `/account/keys`);
  return ssh_keys;
}

export async function createKey(token, name, public_key) {
  const { ssh_key } = await post(token, `/account/keys`, {}, {
    name,
    public_key,
  });
  return ssh_key;
}

export async function removeKey(token, idOrFingerprint) {
  return await remove(token, `/account/keys/${idOrFingerprint}`);
}
