import { get, remove } from "./api.js";

export async function getDropletSnapshots(token) {
  const { snapshots } = await get(token, `/snapshots`, {
    resource_type: "droplet",
  });
  return snapshots;
}

export async function removeSnapshot(token, id) {
  return await remove(token, `/snapshots/${id}`);
}
