import { get, remove } from "./api.js";

export async function getVolumes(token, { name, region } = {}) {
  const { volumes } = await get(token, `/volumes`, { name, region });
  return volumes;
}

export async function removeVolume(token, volume_id) {
  return await remove(token, `/volumes/${volume_id}`);
}
