export async function get(token, path, params) {
  return await request("GET", token, path, params);
}

export async function remove(token, path, params) {
  return await request("DELETE", token, path, params);
}

export async function post(token, path, params, bodyJson) {
  return await request("POST", token, path, params, JSON.stringify(bodyJson));
}

async function request(method, token, path, params = {}, body) {
  const urlParams = {};
  for (const [k, v] of Object.entries(params)) {
    if (v !== undefined) {
      urlParams[k] = v;
    }
  }
  const url = "https://api.digitalocean.com/v2" + path + "?" +
    new URLSearchParams(urlParams);
  const headers = {
    "Content-Type": "application/json",
    "Authorization": `Bearer ${token}`,
  };
  const res = await fetch(url, { method, headers, body });
  const { status, statusText } = res;
  const resp = await res.text();
  if (status < 300) {
    return resp ? await JSON.parse(resp) : undefined;
  } else {
    throw `badresp: ${status} ${statusText} url: ${url} body: ${resp}`;
  }
}
