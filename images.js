import { get, post } from "./api.js";

export async function createImage(
  token,
  name,
  url,
  region,
  { distribution, description, tags } = {},
) {
  const { image } = await post(token, `/images`, {}, {
    name,
    url,
    region,
    distribution,
    description,
    tags,
  });
  return image;
}

export async function getImagesByTag(token, tag_name) {
  const { images } = await get(token, `/images`, { tag_name });
  return images;
}

export async function getImages(token) {
  const { images } = await get(token, `/images`);
  return images;
}

export async function getUserImages(token) {
  const { images } = await get(token, `/images`, { private: true });
  return images;
}
