import { get, post } from "./api.js";
import { getDroplets } from "./droplets.js";

export async function getTags(token) {
  const { tags } = await get(token, `/tags`);
  return tags;
}

export async function createTag(token, name) {
  const { tag } = await post(token, `/tags`, {}, { name });
  return tag;
}

export async function tagDroplet(token, tag, dropletId) {
  return await post(token, `/tags/${tag}/resources`, {}, {
    resources: [{
      resource_id: dropletId.toString(),
      resource_type: "droplet",
    }],
  });
}

export async function tagDropletByName(token, tag, dropletName) {
  await create(token, tag);
  const drops = await getDroplets(token);
  const { id } = drops.filter(({ name }) => name === dropletName)[0];
  return await tagDroplet(token, tag, id);
}
